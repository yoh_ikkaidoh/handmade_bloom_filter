extern crate handmade_bloom_filter;
extern crate rand;
use rand::{Rng,thread_rng};
use rand::seq::SliceRandom;
use std::time::Instant;
const BASES:[u8;4] = [b'A',b'C',b'G',b'T'];

use handmade_bloom_filter::BloomFilter;

fn generate_long_input()->Vec<Vec<u8>>{
    let mut rng = thread_rng();
    (0..100)
        .map(|_|
             (0..rng.gen_range(1000,2000))
             .filter_map(|_| BASES.choose(&mut rng))
             .map(|&e|e)
             .collect()
        )
        .collect()
}

fn generate_huge_input()->Vec<Vec<u8>>{
    let mut rng = thread_rng();
    (0..10000)
        .map(|_|
             (0..rng.gen_range(10000,20000))
             .filter_map(|_| BASES.choose(&mut rng))
             .map(|&e|e)
             .collect()
        )
        .collect()
}

fn strings()->Vec<Vec<u8>>{
    vec![
        b"ACGTCAGTAGCTAGTGTAGCTGTAGCATGCATC".to_vec(),
        b"CCAGTACGTAGCTGGGGGCGATGTGTGTACGTGACAGTGTACTGACTGT".to_vec(),
        b"CAGTCAGGGGCAAACACACAAAACATTGCATGACTAGCATGACTAGCTAGCTATATCGACT".to_vec(),
        b"CAGTACGTGTTGGTTGGTTGGTGTTGTTGACTGACGTCAGTACTGCATGCATGACTG".to_vec(),
    ]
}

#[test]
fn initialization_test(){
    let _bf = BloomFilter::new_with_default(12);
    let _bf = BloomFilter::new_with_configures(173, 12, 14).unwrap();
    let map1 = |x| x ;
    let map2 = |x| x ;
    let _bf = BloomFilter::new_with_full_configures(11,2,10,map1,map2).unwrap();
    let input = strings();
    let _bf = BloomFilter::new_with_dataset(97,12,&input).unwrap();
}

#[test]
fn insertion_test(){
    let k = 12;
    let mut bf = BloomFilter::new_with_default(k);
    let input = strings();
    for line in &input{
        for kmer in line.windows(k){
            assert!(bf.insert(&kmer).is_some());
        }
    }
    for line in &input{
        for kmer in line.windows(k){
            assert_eq!(bf.insert(&kmer),Some(false));
        }
    }
}

#[test]
fn retain_test(){
    let k = 12;
    let mut bf = BloomFilter::new_with_default(k);
    let input = strings();
    for line in &input{
        for kmer in line.windows(k){
            assert!(bf.insert(&kmer).is_some());
        }
    }
    for line in &input{
        for kmer in line.windows(k){
            assert!(bf.has(&kmer));
        }
    }
}


#[test]
fn retain_test_in_large(){
    let k = 16;
    let input = generate_long_input();
    let mut bf = BloomFilter::new_with_configures(105227,12,k).unwrap();
    for line in &input{
        for kmer in line.windows(16){
            assert!(bf.insert(kmer).is_some());
        }
    }
    for line in &input{
        for kmer in line.windows(16){
            assert!(bf.has(kmer));
        }
    }
}

#[test]
fn false_positive_test(){
    let k = 16;
    let modulo = 15486059;
    let input = generate_long_input();
    let mut bf = BloomFilter::new_with_configures(modulo,12,k).unwrap();
    let mut check = std::collections::HashSet::new();
    for line in &input{
        for kmer in line.windows(16){
            assert!(bf.insert(kmer).is_some());
            check.insert(kmer.to_vec());
        }
    }
    let input = generate_long_input();
    let (mut _tp,mut _tn,mut fp,mut fns) = (0,0,0,0);
    let len:usize = input.iter().map(|e|e.len() - k + 1).sum();
    for line in &input{
        for kmer in line.windows(16){
            let ans = check.contains(kmer);
            let pred = bf.has(kmer);
            if ans && pred {
            }else if ans && !pred{
                fns += 1;
            }else if !ans && pred {
                fp += 1;
            }else if !ans && !pred{
            }
        }
    }
    let fp_thr = len * 3 / 1000; // 0.3 % error.
    assert_eq!(fns,0);
    assert!(fp_thr > fp);
    println!("{}",bf);
}

#[test]
fn speedup_test(){
    let k = 16;
    let modulo = 15486059;
    let input = generate_huge_input();

    // HashMap
    let mut check = std::collections::HashSet::new();
    let check_start = Instant::now();
    for line in &input{
        for kmer in line.windows(16){
            check.insert(kmer.to_vec());
        }
    }
    let check_end = Instant::now();

    // Speed check of the Bloom Filter
    let bf_start = Instant::now();
    let mut bf = BloomFilter::new_with_configures(modulo,12,k).unwrap();
    bf.insert_strings(&input);
    let bf_end = Instant::now();

    let len:usize = input.iter().map(|e|e.len() - k).sum();
    let bf_time = bf_end - bf_start;
    let map_time = check_end - check_start;
    println!("{}",bf);
    println!("input length:{}",len);
    println!("BF:{:?} <-> HM:{:?}",bf_time,map_time);
    assert!( bf_time < map_time ,"{:?} vs {:?}",bf_time,map_time);

}

// #[test]
// fn speedcheck_bf(){
//     let k = 16;
//     let modulo = 15486059;
//     let input = generate_huge_input();

//     // Speed check of the Bloom Filter
//     let bf_start = Instant::now();
//     let mut bf = BloomFilter::new_with_configures(modulo,12,k).unwrap();
//     bf.insert_strings(&input);
//     let bf_end = Instant::now();

//     let len:usize = input.iter().map(|e|e.len() - k).sum();
//     let bf_time = bf_end - bf_start;
//     println!("{}",bf);
//     println!("input length:{}",len);
//     println!("BF:{:?}",bf_time);
// }

// #[test]
// fn speedcheck_hashmap(){
//     let k = 16;
//     let input = generate_huge_input();

//     // HashMap
//     let mut check = std::collections::HashSet::new();
//     let check_start = Instant::now();
//     for line in &input{
//         for kmer in line.windows(16){
//             check.insert(kmer.to_vec());
//         }
//     }
//     let check_end = Instant::now();

//     let len:usize = input.iter().map(|e|e.len() - k).sum();
//     let map_time = check_end - check_start;
//     println!("input length:{}",len);
//     println!("HM:{:?}",map_time);
// }


#[test]
fn insertion_test_in_parallel(){
    
}

#[test]
fn insertion_test_in_parallel_speedup(){
    
}

#[test]
fn retain_test_in_parallel(){
}
