//! Minimal implementations of bitvector.
//! It is `bare` bitvector, i.e., it does not have any auxuality functions 
//! along with supplimental data structure to compute `rank` of `select` in constant time.
#[derive(Debug,Clone)]
pub struct BitVector{
    inner:Vec<u64>,
    len:usize,
}

impl std::fmt::Display for BitVector{
    fn fmt(&self,f:&mut std::fmt::Formatter)->std::fmt::Result{
        writeln!(f,"BitVec. Length:{}. {} Ocuppied.",self.len(),self.count_ones())
    }
}

impl BitVector{
    pub fn new()->Self{
        let len :usize= 0;
        let inner = vec![];
        BitVector{inner,len}
    }
    #[inline]
    fn len(&self)->usize{
        self.len
    }
    // If the bv[i] == 1 already, return false, otherwise, return true(add successfully).
    #[inline]
    pub fn add(&mut self,i:usize)->bool{
        if i >= self.inner.len() * 64{
            self.inner.extend(vec![0;i + 1 - self.inner.len() * 64]);
        }
        let (bucket,position) = (i/64,i%64);
        let adder = 1 << position;
        let not_already_in = self.inner[bucket] & adder == 0;
        self.inner[bucket] |= adder;
        self.len = self.len.max(i);
        not_already_in
    }
    #[inline]
    pub fn has(&self,i:usize)->bool{
        i <= self.len && {
            let (bucket,position) = (i/64,i%64);
            let probe = 1 << position;
            self.inner[bucket] & probe != 0
        }
    }
    #[inline]
    pub fn count_ones(&self)->u64{
        self.inner.iter().map(|e|e.count_ones() as u64).sum()
    }
}

#[cfg(test)]
mod tests{
    use super::*;
    #[test]
    fn initialize_test(){
        let _bv = BitVector::new();
    }
    #[test]
    fn insert_test(){
        let mut bt = BitVector::new();
        bt.add(12);
    }
    #[test]
    fn retain_test(){
        let mut bt = BitVector::new();
        bt.add(12);
        assert!(bt.has(12));
        assert!(!bt.has(0));
        assert!(!bt.has(100));
        assert_eq!(bt.len(),12);
    }
    #[test]
    fn double_insert_test(){
        let mut bt = BitVector::new();
        bt.add(12);
        bt.add(12);
        assert!(bt.has(12));
        assert!(!bt.has(0));
        assert!(!bt.has(100));
    }
    #[test]
    fn multiple_insert_test(){
        let mut bt = BitVector::new();
        let inserts = vec![0,64,128,32,129,32];
        for i in &inserts{
            eprintln!("{}",bt);
            bt.add(*i);
        }
        for i in &inserts{
            assert!(bt.has(*i));
        }
        assert!(!bt.has(2324234));
        assert!(!bt.has(1));
        assert_eq!(bt.len(),129);
    }
    #[test]
    fn count_ones_test(){
        let mut bt = BitVector::new();
        let inserts = vec![0,64,128,32,129,32];
        for i in &inserts{
            eprintln!("{}",bt);
            bt.add(*i);
        }
        assert_eq!(bt.count_ones(),5)
    }
}
